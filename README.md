=== Plugin Name ===
LinklayEmbed

Contributors:      
Plugin Name:       Linklay Embed
Plugin URI:        
Tags:              shortcode, iframe, linklay, shoppable image
Author URI:        
Author:            
Donate link:       
Requires PHP:      5.4
Requires at least: 1.9.3
Tested up to:      1.9.3
Stable tag:        
Version:           0.1.0

== Description ==

The module can be disabled from the settings page. 
*Important:* The module is disabled by default. If you leave the _Enabled_ box unchecked, the plugin will have no effect at all. 

Example: {id} => linklay59fff9229e4fe6.97620297
Pattern: {{block type="" image_hash="" }}
Shortcodes with the pattern above will display the referenced Linklay shoppable image .

Shortcode configuration:

The following attributes can be used to configured the display of the iframe and/or its wrapper:

* class
    * A CSS class that will be applied to an outer wrapper (div). Defaults to none.
    * Example: {{block type="shortcodeembed/shortcodeembed" name="shortcodeembed_shortcodeembed" align="right" class="linklayShopping" 
    *             image_hash="linklay5a0e064a5acef5.23861345"  template="shortcodeembed/shortcodeembed.phtml"}}
* align
    * Horizontal alignment of the linklay image within the container: left, center or right. Default: center. The _align_ parameter has no effect if the original image is as wide or wider than the parent element.

== Installation ==

Install and configure the plugin as needed in the LinklayEmbed settings page: /wp-admin/admin.php?page=linklayembed_settings. 

_Note_ that the plugin is not enabled by default. Check the _Enabled_ box in the settings page to turn it on.

 
== UpgradGenerale Notice ==
= 0.1.0 =
Initial release

== Screenshots ==


== Changelog ==
= 0.0 =
* Initial release.

== Roadmap ==

== Frequently Asked Questions ==

== Donations ==
